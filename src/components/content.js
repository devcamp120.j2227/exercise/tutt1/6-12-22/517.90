import { Component } from "react";
import Input from "./input";
import Output from "./output";

class Content extends Component {
    render() {
        return (
            <div className="container">
                <div className="row text-center">
                    <div className="col-sm-12">
                        <Input/>
                    </div>
                </div>
                <div className="row text-center">
                    <div className="col-sm-12">
                        <Output/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Content