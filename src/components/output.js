import { Component } from "react";

let hour = new Date().getHours();
let minute = new Date().getMinutes();
let second = new Date().getSeconds();
let statusDay = hour <= 12 ? "AM" : "PM";

class Output extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: []
        }
    }
    addClick = () => {
        let hour = new Date().getHours();
        let minute = new Date().getMinutes();
        let second = new Date().getSeconds();
        let statusDay = hour <= 12 ? "AM" : "PM";
        let timeAdd = "";
        timeAdd = `Time is: ${hour} : ${minute} : ${second} - ${statusDay}`
        this.setState({
            time: [...this.state.time, timeAdd]
        })
    }
    render() {
        return (
            <div className="mt-4">
                <h4>Time is: {hour} : {minute} : {second} - {statusDay}</h4>
                <div className="row mt-3">
                    {this.state.time.map((element, index) => {
                        return <h4 key={index}>{element}</h4>
                    })}
                </div>
                <button className="btn btn-success mt-4" onClick={() => this.addClick()}>Add to list</button>
            </div>
        )
    }
}

export default Output