import Content from './components/content';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Content/>
    </div>
  );
}

export default App;
